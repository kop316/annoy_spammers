#!/bin/bash
# Sleep time is in seconds
max_sleep_time=120
password_file='password.txt'
domain_file='domains.txt'
username_file='usernames.txt'
forename_file='Forename_Autumn2014.txt'
surname_file='Surname_Autumn2014.txt'
#TODO: Fashion these together in a useable way to make fake usernames
user_agent_file='user-agent.txt'
# https://svbridalcanvas.com/dyuwhhdgjhbbpldhbsbayghsvbcmmdhalkmshdggghsghdg/gheyttwgsnmmmmbxvsjzklhdtghvaipxajkdopktwgbaioplsnba.php#test@example.com
# ^^ This is the URL I get in the phishing email. Obviously not the right email though. 
#url='https://serenityplacefosterservices.com/send.php'
#origin='Origin: https://curowaycreations.com'
#referer='Referer: https://curowaycreations.com/'
#email_payload='email='
#password_payload='password='
#include_payload_in_url=0

# https://macorbox.com/cpanelwebmail-administration/Webmail.html#test@example.com
#url='https://canarybeachhotel.sa/htmlweb/send/webmail.php'
#origin='Origin: https://macorbox.com'
#referer='Referer: https://macorbox.com/'
#email_payload='user='
#password_payload='pass='
#include_payload_in_url=1

#https://ipfs.io/ipfs/QmXLBTn2EMiDTYimRsjXSzAwHjdcEDbvvA87eveXh3REMC?filename=QmXLBTn2EMiDTYimRsjXSzAwHjdcEDbvvA87eveXh3REMC#test@example.com
#url='https://inkxln.com/bins/eenext.php'
#origin='Origin: https://ipfs.io'
#referer='Referer: https://ipfs.io/'
#email_payload='email='
#password_payload='password='
#include_payload_in_url=0

#https://firebasestorage.googleapis.com/v0/b/newmani.appspot.com/o/gown22%2FABAN%40NAA.shtml?alt=media&token=6ae30e55-a9ea-4d3f-b75d-280b9e5eeaf7#test@test.com
#url='https://winepress.top/clear/fire.php'
#origin='Origin: https://firebasestorage.googleapis.com'
#referer='Referer: https://firebasestorage.googleapis.com/'
#email_payload='user='
#password_payload='pass='
#include_payload_in_url=0

#https://firebasestorage.googleapis.com/v0/b/newmani.appspot.com/o/gown22%2FABAN%40NAA.shtml?alt=media&token=6ae30e55-a9ea-4d3f-b75d-280b9e5eeaf7#test@test.com
#url='https://tulipsisp.com/yFG--HELUJiAntIf--cVOZZY/BVMng--FYYxoIBO--HiPGPNQ/ibNfIfRkkSkXLeGMIzqY/general-page-loan.php'
#origin='Origin: https://tulipsisp.com'
#referer='Referer: https://tulipsisp.com/yFG--HELUJiAntIf--cVOZZY/BVMng--FYYxoIBO--HiPGPNQ/index.html'
#email_payload='email='
#password_payload='password='
#include_payload_in_url=0

#http://mmail.youxiangjfs.club/Index/index2/?wanziman.com/?test@test.com
#lol I look down this site
#url='http://mmail.youxiangjfs.club/Index/login'
#origin='Origin: http://mmail.youxiangjfs.club'
#referer_to_use='Referer: http://mmail.youxiangjfs.club/Index/index2/?wanziman.com/?'
#include_email_in_referer=1
#email_payload='user='
#password_payload='password='
#include_payload_in_url=0

#https://ipfs.io/ipfs/Qmc2MsBBHwJuNXr5TH6qrEcWE1WVnh3wG7njz4JPFkheYC?filename=Qmc2MsBBHwJuNXr5TH6qrEcWE1WVnh3wG7njz4JPFkheYC#test@test.com
url='https://inkxln.com/vicdp/xznext.php'
origin='Origin: https://ipfs.io'
referer='Referer: https://ipfs.io/'
email_payload='email='
password_payload='password='
include_payload_in_url=0

#username_num_lines=0
username_num_lines=8295455
domain_num_lines=0
#domain_num_lines=21
#password_num_lines=0
password_num_lines=999998
#user_agent_num_lines=0
user_agent_num_lines=50

# If you want to use your own list, you need to calculate line numbers. I use one value per line
if [ $username_num_lines = 0 ]; then
  echo "Calculating number of Usernames"
  while IFS= read -r line; do
    ((username_num_lines=username_num_lines+1))
  done <$username_file
  
  echo "Number of lines in username"
  echo $username_num_lines
fi

if [ $domain_num_lines = 0 ]; then
  while IFS= read -r line; do
    ((domain_num_lines=domain_num_lines+1))
  done <$domain_file

  echo "Number of lines in domain"
  echo $domain_num_lines
fi

if [ $password_num_lines = 0 ]; then
  while IFS= read -r line; do
    ((password_num_lines=password_num_lines+1))
  done <$password_file
  
  echo "Number of lines in password"
  echo $password_num_lines
fi

if [ $user_agent_num_lines = 0 ]; then
  while IFS= read -r line; do
    ((user_agent_num_lines=user_agent_num_lines+1))
  done <$user_agent_file
  
  echo "Number of lines in user_agent"
  echo $user_agent_num_lines
fi

while :
do
  username_line=$(source ${BASH_SOURCE%/*}/rand.sh 1 $username_num_lines)
  username_to_use=$(sed $username_line'!d' $username_file)

  domain_line=$(source ${BASH_SOURCE%/*}/rand.sh 1 $domain_num_lines)
  domain_to_use=$(sed $domain_line'!d' $domain_file)

  email_to_use=$username_to_use'@'$domain_to_use
  echo "Email to use:" $email_to_use

  password_line=$(source ${BASH_SOURCE%/*}/rand.sh 1 $password_num_lines)
  password_to_use=$(sed $password_line'!d' $password_file)

  echo "Password to use:" $password_to_use
 
  curl_command=$email_payload$email_to_use'&'$password_payload$password_to_use
  # This only works with:
  #http://mmail.youxiangjfs.club/Index/index2/?wanziman.com/?test@test.com
  #curl_command=$email_payload$username_to_use'&suffix=@'$domain_to_use'&'$password_payload$password_to_use'&t='$(date +%s)
  # This only works with:
  #http://mmail.youxiangjfs.club/Index/index2/?wanziman.com/?test@test.com
#   if [ $include_email_in_referer -eq 1 ]; then
#    referer="$referer_to_use$email_to_use"
#    echo "Referer:" $referer
#  fi

  echo "Curl Command:" $curl_command

  user_agent_line=$(source ${BASH_SOURCE%/*}/rand.sh 1 $user_agent_num_lines)
  user_agent_to_use=$(sed $user_agent_line'!d' $user_agent_file)

  echo "user_agent to use:" $user_agent_to_use
  if [ $include_payload_in_url -eq 0 ]; then
    url_to_use=$url
  else
    url_to_use=$url'?'$curl_command
  fi
  echo "Url:" $url_to_use

  curl -d $curl_command -A "$user_agent_to_use" -H "$origin" -H "$referer" $url_to_use -v --connect-timeout 5 --max-time 10 --ciphers DEFAULT:@SECLEVEL=1
  # The site itself gives you a 500, and makes you think something went wrong.
  # Let's send it twice to simulate a user thinking something went wrong
  # After the second time, it redirects to a different site, so three times
  # doesn't make sense
  sleep_time=$(source ${BASH_SOURCE%/*}/rand.sh 10 20)
  echo ""
  echo "Sleeping for (sec) before second entry" $sleep_time
  sleep $sleep_time
  curl -d $curl_command -A "$user_agent_to_use" -H "$origin" -H "$referer" $url_to_use -v --connect-timeout 5 --max-time 10 --ciphers DEFAULT:@SECLEVEL=1

  sleep_time=$(source ${BASH_SOURCE%/*}/rand.sh 60 $max_sleep_time)
  echo ""
  echo "Sleeping for (sec)" $sleep_time
  sleep $sleep_time
done
