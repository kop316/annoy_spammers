# Annoy_spammers

Since I have contributed to open source. I keep getting spammed by phishing attacks.
I have tried to report them, but that is an annoyingly lengthy process that does
not seem to help.

Since they want email and password credentials, let's poison the well!

I right now have a simple bash script that calls cURL to post to the website
(I extracted it through Chrome's devtools). I call on the lists here to randomize
usernames, passwords, user agents, and email domains to make it harder to correlate out.

Password list and username list from: https://github.com/danielmiessler/SecLists/

Alt_domains from: https://gist.github.com/tbrianjones/5992856

User Agent from: https://github.com/yusuzech/top-50-user-agents/blob/master/user_agent.csv

Surnames, forenames from: https://github.com/matt40k/Names

You will need cURL for this, and it runs in bash (but probably works in other shells).
